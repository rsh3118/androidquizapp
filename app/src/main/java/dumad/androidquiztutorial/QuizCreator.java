package dumad.androidquiztutorial;

import java.util.ArrayList;

public class QuizCreator {
    public static Quiz createPokemonQuiz(){
        Question[] pokemonQuestions = new Question[3];

        ArrayList<Answer> pokemonWrongAnswers = new ArrayList<Answer>();
        pokemonWrongAnswers.add(new Answer("Alakazam", "https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Alakazam.png?alt=media&token=c3f7de0c-bc76-4db9-a2b4-c80ad93b5749"));
        pokemonWrongAnswers.add(new Answer ("Crobat","https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Crobat.png?alt=media&token=511b9f58-aa8b-4d6d-a073-729bcc661889"));
        pokemonWrongAnswers.add(new Answer ("Dragonite","https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Dragonite.png?alt=media&token=4a795769-2725-4b30-80f9-21e6fbe6685c"));
        pokemonWrongAnswers.add(new Answer ("Dusknoir","https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Dusknoir.png?alt=media&token=910502b1-c775-4b7e-8144-5d92c53503c5"));
        Answer pokemonRightAnswer = new Answer("Aggron", "https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Aggron.png?alt=media&token=7ade9d89-c454-42c8-b80b-bd4dfc3aaf54");
        Question pokemonQuestion = new Question();
        pokemonQuestion.wrongAnswers = pokemonWrongAnswers;
        pokemonQuestion.correctAnswer = pokemonRightAnswer;
        pokemonQuestion.question = "Which pokemon is steel type?";
        pokemonQuestions[0] = pokemonQuestion;

        ArrayList<Answer> pokemonWrongAnswers2 = new ArrayList<Answer>();
        pokemonWrongAnswers2.add(new Answer("Electivire","https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Electivire.png?alt=media&token=0a9b0d1a-27d2-413a-b64c-83e966f60c7f"));
        pokemonWrongAnswers2.add(new Answer("Exploud","https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Exploud.png?alt=media&token=9c67d0ad-de07-44b6-94e9-001da39587f2"));
        pokemonWrongAnswers2.add(new Answer("Garchomp","https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Garchomp.png?alt=media&token=a48f8c65-723a-4a81-a4fb-2e42a725ad92"));
        pokemonWrongAnswers2.add(new Answer("Gengar", "https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Gengar.png?alt=media&token=b2b52e82-4493-4163-96dc-3caba5b2916c"));
        pokemonWrongAnswers2.add(new Answer("Golem","https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Golem.png?alt=media&token=12b74973-dd3c-4b71-a03e-aa0c22de7441"));
        pokemonWrongAnswers2.add(new Answer("Haxorus", "https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Haxorus.png?alt=media&token=0b929325-4c22-4ab6-bfb2-d82bb241eaab"));
        pokemonWrongAnswers2.add(new Answer("Kingdra","https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Kingdra.png?alt=media&token=f79e0927-5333-4394-8864-4a9c6288bb95"));
        pokemonWrongAnswers2.add(new Answer("Luxray", "https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Luxray.png?alt=media&token=d6ad8b2e-48d1-4aaa-a293-43d987370f5a"));
        pokemonWrongAnswers2.add(new Answer("Machamp", "https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Machamp.png?alt=media&token=debbfaca-907b-4d3c-bcd1-48d3e95ee41f"));
        pokemonWrongAnswers2.add(new Answer("Magmortar", "https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Magmortar.png?alt=media&token=9b10843a-6feb-4298-b7cb-fa96b292bd2b"));
        Answer pokemonRightAnswer2 = new Answer("Magnezone","https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Magnezone.png?alt=media&token=3dd5b826-22e1-46c6-b3d4-4fff7bf7dd87");
        Question pokemonQuestion2 = new Question();
        pokemonQuestion2.wrongAnswers = pokemonWrongAnswers2;
        pokemonQuestion2.correctAnswer = pokemonRightAnswer2;
        pokemonQuestion2.question = "Which pokemon is weak to fire?";
        pokemonQuestions[1] = pokemonQuestion2;

        ArrayList<Answer> pokemonWrongAnswers3 = new ArrayList<Answer>();
        pokemonWrongAnswers3.add(new Answer("Metagross","https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Metagross.png?alt=media&token=cf977437-bb7c-48f4-909c-1e241433c0f8"));
        pokemonWrongAnswers3.add(new Answer("PorygonZ","https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/PorygonZ.png?alt=media&token=4ceb8b08-4782-4747-9c51-5e74059ffec4"));
        pokemonWrongAnswers3.add(new Answer("Rhyperior","https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Rhyperior.png?alt=media&token=87196de3-77ea-4542-b0fc-5107838464eb"));
        Answer pokemonRightAnswer3 = new Answer("Salamence","https://firebasestorage.googleapis.com/v0/b/quizapp-26149.appspot.com/o/Rhyperior.png?alt=media&token=87196de3-77ea-4542-b0fc-5107838464eb");
        Question pokemonQuestion3 = new Question();
        pokemonQuestion3.wrongAnswers = pokemonWrongAnswers3;
        pokemonQuestion3.correctAnswer = pokemonRightAnswer3;
        pokemonQuestion3.question = "Which pokemon can fly?";
        pokemonQuestions[2] = pokemonQuestion3;


        return new Quiz("Pokemon Quiz", pokemonQuestions);
    }
}
