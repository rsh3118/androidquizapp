package dumad.androidquiztutorial;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Ritler on 9/30/17.
 */

public class Quiz {
    ArrayList<Question> questions;
    String title;

    public Quiz(){

    }
    public Quiz(String title, Question[] questionsArray) {
        title = title;
        questions = new ArrayList<>(Arrays.asList(questionsArray));
    }


    public String getTitle(){
        return title;
    }

    public int size(){
        return questions.size();
    }

    public Question getQuestion(int index) {
        if (0 <= index && index < questions.size()) {
            return questions.get(index);
        }
        throw new IndexOutOfBoundsException("bad index " + index);
    }
}
