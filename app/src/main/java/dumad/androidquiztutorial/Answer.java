package dumad.androidquiztutorial;

/**
 * Created by Ritler on 9/30/17.
 */

public class Answer {
    public String answerText;
    public String answerImage;

    public Answer(){}

    public Answer(String aT, String aI){
        answerText = aT;
        answerImage = aI;
    }
}
